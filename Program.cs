﻿using System;

namespace CardDeck
{
    class Program
    {
        static void Main(string[] args)
        {
            Deck deck = new Deck();

            while(true)
            {
                var input = Console
                    .ReadLine()
                    .ToUpper()
                    .Trim();

                switch(input)
                {
                    case "Q":
                        {
                            Console.WriteLine("Goodbye!");
                            return;
                        }

                    case "N":
                        {
                            Console.WriteLine("New deck created!");
                            deck = new Deck();
                            break;
                        }

                    case "S":
                        {
                            Console.WriteLine("Deck has been shuffled");
                            deck.Shuffle();
                            break; 
                        }

                    case "D":
                        {
                            var card = deck.Draw();
                            Console.WriteLine($"Card Drawn: {card}");
                            break;
                        }

                    case "R":
                        {
                            Console.WriteLine("Cards in deck:");

                            foreach (var card in deck.Cards)
                            {
                                Console.WriteLine(card);
                            }
                            break;
                        }

                    default:
                        {
                            Console.WriteLine("Unknown input");
                            break;
                        }
                }
            }
        }
    }
}
