﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CardDeck
{
    /// <summary>
    /// Represents a deck of cards.
    /// Deck must be shuffled before use.
    /// </summary>
    public class Deck
    {
        private readonly Random randomNumberGenerator;
        private Queue<Card> deck;

        public Deck()
        {
            randomNumberGenerator = new Random();

            var ranks = Enumerable
                .Range(2, 13)
                .Select(rank => (Rank)rank);

            var suits = Enumerable
                .Range(0, 4)
                .Select(suit => (Suit)suit);

            var unshuffledCards = from suit in suits
                       from rank in ranks
                       select new Card
                       {
                           Suit = suit,
                           Rank = rank
                       };

            deck = new Queue<Card>(unshuffledCards);
        }

        public IEnumerable<Card> Cards
            => deck.AsEnumerable();

        /// <summary>
        /// Returns the next card from the deck.
        /// If deck contains no more cards, this method returns null
        /// </summary>
        public Card Draw()
        {
            if (deck.Any())
            {
                return deck.Dequeue();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Shuffles the deck
        /// </summary>
        public void Shuffle()
        {
            var cards = deck.ToArray();

            // Fisher-Yates shuffle
            int n = cards.Count();
            while (n > 1)
            {
                n--;
                int k = randomNumberGenerator.Next(n + 1);
                Card card = cards[k];
                cards[k] = cards[n];
                cards[n] = card;
            }

            deck = new Queue<Card>(cards);
        }
    }
}
