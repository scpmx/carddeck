﻿using System;

namespace CardDeck
{
    /// <summary>
    /// 
    /// </summary>
    public class Card
    {
        public Suit Suit { get; set; }

        public Rank Rank { get; set; }

        public override string ToString()
        {
            var suit = Enum.GetName(typeof(Suit), Suit);
            var rank = Enum.GetName(typeof(Rank), Rank);

            return $"{rank} of {suit}";
        }
    }
}
